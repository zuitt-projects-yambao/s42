const txtFirstName = document.querySelector("#txt-first-name");
const txtLastName = document.querySelector("#txt-last-name");

console.log(txtLastName)

const spanFullName = document.querySelector("#span-full-name");

const fullName = () => {
	let firstName = txtFirstName.value;
	let lastName = txtLastName.value;

	spanFullName.innerHTML = `${firstName} ${lastName}`
}

txtLastName.addEventListener('keyup', fullName)
txtFirstName.addEventListener('keyup', fullName)